#include <iostream>
#include <fstream>
#include <string>

#include <google/protobuf/util/time_util.h>
#include "./proto/person.pb.h"

using namespace std;

using google::protobuf::util::TimeUtil;

int main(int argc, char* argv[]){

    fstream myfile;
    // Open a file
    myfile.open("setup.md");
    myfile << "# Hello World (Protobuffer)!" << endl;
    myfile.close();
    
    // Read the file
    string line;
    myfile.open("setup.md");
    if(myfile.is_open()){
         while ( getline (myfile, line) ){
             cout << line << endl;
         }
         myfile.close();
    } else {
        cout << "Unable to open the file" << endl;
    }
    string str;

    // Protobuffer
    tutorial::Person person;
    person.set_id(0);
    person.set_name("Luca Polverini");

    fstream output("./output/person-serialized", ios::out | ios::trunc | ios::binary);
    person.SerializeToOstream(&output);
    myfile.close();

    cout << "----------------------------------------" << endl;
    cout << "Protobuffer Person" << endl;
    cout << "----------------------------------------" << endl;
    cout << "id    : " << person.id() << endl;
    cout << "name  : " << person.name() << endl;
    cout << "email : " << person.email() << endl;
    cout << "----------------------------------------" << endl;

    google::protobuf::ShutdownProtobufLibrary();

    return 0;
}