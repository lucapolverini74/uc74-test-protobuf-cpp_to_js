var Proto = require('../proto/person_pb');

var person = new Proto.Person();
person.setId(0);
person.setName("Luca Polverini");

var bytes = person.serializeBinary();
console.log(bytes);

console.log(Proto.Person.deserializeBinary(bytes));

var fs = require('fs');
fs.readFile("/Users/lp74/uc74-test-protobuf/output/person-serialized", function (err, b) {
    console.log('file buffer =>', b);
    console.log(
        'Person from person serialized:',
        Proto.Person.deserializeBinary(
            new Uint8Array(b.buffer, b.byteOffset, b.byteLength / Uint8Array.BYTES_PER_ELEMENT)
        )
    );
});
