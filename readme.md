
# Project Goals

The goals of this simple project are:

- [x] To compile a protobuffer using c++ and javascript, :smile:

- [x] Serialize a message in c++, and :smile:

- [x] Read the file serialized in c++ from the javascript app. :smile:

# How to compile

Download the protobuf source from [github](https://github.com/google/protobuf/releases) and compile it. On macOS High Sierra:

```
# extract the archive
$ tar -xvf protobuf-3.5.1

# compile for macOS High Sierra
$ ./configure
$ make
$ make check
$ sudo make install 
```

Finally, on Linux, you should run:
```
$ sudo ldconfig
```

Inside the protobuf folder there is the js folder:

```bash
$ cd js

$ npm install

$ npm test

# if protoc is elsewhere:
$ PROTOC=/usr/local/bin/protoc npm test

# to build google-protobuf.js
$ gulp dist
```

## C++

### Protobuffer
```bash
$ protoc -I=./ --cpp_out=. ./proto/person.proto
```

### main.cpp
```bash
$ g++ -std=c++11 -I ./proto -L /usr/local/lib/ -l protobuf -Wall -o2 main.cpp ./proto/person.pb.cc -o ./bin/main.o

# now run main.o to generate the serialized file
$ ./bin/main.o 
```

## Javascript

Currently, **copy** the google-protobuf.js file inside the node_modules folder.
It should be installed via npm.

```bash
$ protoc --js_out=import_style=commonjs,binary:. ./proto/person.proto

# now run the app
$ node /js/app.js
```
